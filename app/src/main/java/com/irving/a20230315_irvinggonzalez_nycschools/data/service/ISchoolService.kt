package com.irving.a20230315_irvinggonzalez_nycschools.data.service

import com.irving.a20230315_irvinggonzalez_nycschools.data.model.SchoolDTO
import com.irving.a20230315_irvinggonzalez_nycschools.data.model.SchoolSATDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ISchoolService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(
        @Query("dbn") dbn: String?
    ): List<SchoolDTO>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSat(
        @Query("dbn") dbn: String?
    ): List<SchoolSATDTO>
}