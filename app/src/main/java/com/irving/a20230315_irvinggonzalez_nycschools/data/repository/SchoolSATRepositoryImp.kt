package com.irving.a20230315_irvinggonzalez_nycschools.data.repository

import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.data.mapper.toSchoolSat
import com.irving.a20230315_irvinggonzalez_nycschools.data.service.ISchoolService
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolSATRepository
import javax.inject.Inject

class SchoolSATRepositoryImp @Inject constructor(private val service: ISchoolService): ISchoolSATRepository {

    override suspend fun getSchoolScores(dbn: String): ResponseHandler<List<SchoolSAT>> {
        return try {
            ResponseHandler.Success(service.getSat(dbn).map { it.toSchoolSat() })
        } catch (e: Exception){
            ResponseHandler.Error(e.message.toString())
        }
    }
}