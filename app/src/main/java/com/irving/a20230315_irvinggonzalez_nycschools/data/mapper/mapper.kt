package com.irving.a20230315_irvinggonzalez_nycschools.data.mapper

import com.irving.a20230315_irvinggonzalez_nycschools.data.model.SchoolDTO
import com.irving.a20230315_irvinggonzalez_nycschools.data.model.SchoolSATDTO
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT

fun SchoolDTO.toSchool(): School{
    return School(
        dbn = dbn.toString(),
        name = school_name.toString(),
        description = overview_paragraph.toString(),
        location = location.toString(),
        phone = phone_number.toString(),
        email = school_email.toString(),
    )
}

fun SchoolSATDTO.toSchoolSat(): SchoolSAT{
    return SchoolSAT(
        dbn = dbn.toString(),
        name = school_name.toString(),
        satTestTaken = num_of_sat_test_takers.toString(),
        readingScore = sat_critical_reading_avg_score.toString(),
        mathScore = sat_math_avg_score.toString(),
        writingScore = sat_writing_avg_score.toString()
    )
}