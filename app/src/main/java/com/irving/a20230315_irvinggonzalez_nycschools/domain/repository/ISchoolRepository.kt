package com.irving.a20230315_irvinggonzalez_nycschools.domain.repository

import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School

interface ISchoolRepository {
    suspend fun getAllSchools(): ResponseHandler<List<School>>
    suspend fun getSchoolById(dbn: String): ResponseHandler<List<School>>
}