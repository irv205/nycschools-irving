package com.irving.a20230315_irvinggonzalez_nycschools.domain.model

data class School(
    val dbn: String = "",
    val name: String = "",
    val description: String = "",
    val location: String = "",
    val phone: String = "",
    val email: String = "",
    val website: String = "",
)