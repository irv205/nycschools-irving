package com.irving.a20230315_irvinggonzalez_nycschools.core.di

import com.irving.a20230315_irvinggonzalez_nycschools.data.repository.SchoolRepositoryImp
import com.irving.a20230315_irvinggonzalez_nycschools.data.repository.SchoolSATRepositoryImp
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolRepository
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolSATRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideSchoolRepository(schoolRepositoryImp: SchoolRepositoryImp): ISchoolRepository

    @Binds
    @Singleton
    abstract fun provideSchoolSATRepository(schoolSATRepositoryImp: SchoolSATRepositoryImp): ISchoolSATRepository
}