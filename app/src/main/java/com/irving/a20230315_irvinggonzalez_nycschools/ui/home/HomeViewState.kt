package com.irving.a20230315_irvinggonzalez_nycschools.ui.home

sealed class HomeViewState {
    object Idle: HomeViewState()
    object Success: HomeViewState()
    class Error(val error: String): HomeViewState()
}