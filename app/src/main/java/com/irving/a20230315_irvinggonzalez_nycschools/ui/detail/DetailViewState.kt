package com.irving.a20230315_irvinggonzalez_nycschools.ui.detail

sealed class DetailViewState {
    object Idle: DetailViewState()
    object Success: DetailViewState()
    object Empty: DetailViewState()
    class Error(val error: String): DetailViewState()
}