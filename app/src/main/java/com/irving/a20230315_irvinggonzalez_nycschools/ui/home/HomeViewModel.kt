package com.irving.a20230315_irvinggonzalez_nycschools.ui.home

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: ISchoolRepository): ViewModel() {

    private val _state = mutableStateOf<HomeViewState?>(HomeViewState.Idle)
    val state : State<HomeViewState?> get() = _state

    private val _listOfSchools = mutableStateListOf<School>()
    val listOfSchools : SnapshotStateList<School> get() = _listOfSchools

    private val _loadingIndicator = mutableStateOf(true)
    val loadingIndicator : State<Boolean> get() = _loadingIndicator

    fun getListOfSchools() {
        _loadingIndicator.value = true
        viewModelScope.launch(Dispatchers.IO) {
            when(val result = repository.getAllSchools()){

                is ResponseHandler.Error -> {
                    _state.value = HomeViewState.Error(result.message)
                }
                is ResponseHandler.Success -> {
                    _state.value = HomeViewState.Success
                    _listOfSchools.clear()
                    _listOfSchools.addAll(result.data)
                }
            }
            _loadingIndicator.value = false
        }
    }
}