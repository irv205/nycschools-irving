package com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun RoundedCornerSATCard(modifier: Modifier, satType: String, score: String){
    Card(elevation = 4.dp,
        modifier = modifier
        .width(50.dp)
        .height(60.dp), backgroundColor = Color.White
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = satType, style = MaterialTheme.typography.subtitle1)
            Spacer(modifier = Modifier.size(5.dp))
            Text(text = score, style = MaterialTheme.typography.subtitle2)
        }
    }
}