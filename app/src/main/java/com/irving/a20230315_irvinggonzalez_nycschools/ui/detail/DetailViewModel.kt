package com.irving.a20230315_irvinggonzalez_nycschools.ui.detail

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolRepository
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolSATRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val schoolSATRepository: ISchoolSATRepository,
    private val schoolRepository: ISchoolRepository,
    private val savedStateHandle: SavedStateHandle
    ): ViewModel() {

    val dbn : String = savedStateHandle["dbn"] ?: ""

    private val _state = mutableStateOf<DetailViewState?>(DetailViewState.Idle)
    val state : State<DetailViewState?> get() = _state

    private val _schoolSAT = mutableStateOf<SchoolSAT?>(null)
    val schoolSAT : State<SchoolSAT?> get() = _schoolSAT

    private val _school = mutableStateOf<School?>(null)
    val school : State<School?> get() = _school

    fun getSchoolSAT(){
        viewModelScope.launch(Dispatchers.IO) {
            when(val result = schoolSATRepository.getSchoolScores(dbn)){

                is ResponseHandler.Error -> {
                    _state.value = DetailViewState.Error(result.message)
                }
                is ResponseHandler.Success -> {
                    result.data?.let {
                        if (it.isNotEmpty()){
                            _schoolSAT.value = it.first()
                            _state.value = DetailViewState.Success
                        } else _state.value = DetailViewState.Empty
                    }
                }
            }
        }
    }

    fun getSchoolByID() {

        viewModelScope.launch(Dispatchers.IO) {
            when(val result = schoolRepository.getSchoolById(dbn)){

                is ResponseHandler.Error -> {
                    _state.value = DetailViewState.Error(result.message)
                }
                is ResponseHandler.Success -> {
                    result.data?.let {
                        if (it.isNotEmpty()){
                            _school.value = result.data.first()
                        }
                    }
                }
            }
        }
    }
}